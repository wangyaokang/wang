(function () {
    var n = ["g", "4", "7", "k", "U", "W", "3", "A", "+", "/", "C", "O", "r", "P", "Z", "j", "Q", "F", "I", "D", "b", "o", "J", "E", "X", "B", "8", "G", "z", "h", "c", "L", "V", "Y", "N", "e", "1", "f", "p", "S", "6", "d", "2", "v", "a", "y", "w", "m", "u", "5", "l", "H", "s", "T", "q", "n", "t", "i", "R", "0", "K", "M", "9", "x"];
    var o = function (a) {
        var c = new Array();
        while (a > 0) {
            var b = a % 2;
            a = Math.floor(a / 2);
            c.push(b)
        }
        c.reverse();
        return c
    };
    var q = function (a) {
        var c = 0;
        var p = 0;
        for (var i = a.length - 1; i >= 0; --i) {
            var b = a[i];
            if (b == 1) {
                c += Math.pow(2, p)
            }++p
        }
        return c
    };
    var r = function (c, a) {
        var b = (8 - (c + 1)) + ((c - 1) * 6);
        var d = a.length;
        var e = b - d;
        while (--e >= 0) {
            a.unshift(0)
        }
        var f = [];
        var g = c;
        while (--g >= 0) {
            f.push(1)
        }
        f.push(0);
        var i = 0,
            len = 8 - (c + 1);
        for (; i < len; ++i) {
                f.push(a[i])
            }
        for (var j = 0; j < c - 1; ++j) {
                f.push(1);
                f.push(0);
                var h = 6;
                while (--h >= 0) {
                    f.push(a[i++])
                }
            }
        return f
    };
    var s = {
        encoder: function (a) {
            var b = [];
            var c = [];
            for (var i = 0, len = a.length; i < len; ++i) {
                var d = a.charCodeAt(i);
                var e = o(d);
                if (d < 0x80) {
                    var f = 8 - e.length;
                    while (--f >= 0) {
                        e.unshift(0)
                    }
                    c = c.concat(e)
                } else if (d >= 0x80 && d <= 0x7FF) {
                    c = c.concat(r(2, e))
                } else if (d >= 0x800 && d <= 0xFFFF) {
                    c = c.concat(r(3, e))
                } else if (d >= 0x10000 && d <= 0x1FFFFF) {
                    c = c.concat(r(4, e))
                } else if (d >= 0x200000 && d <= 0x3FFFFFF) {
                    c = c.concat(r(5, e))
                } else if (d >= 4000000 && d <= 0x7FFFFFFF) {
                    c = c.concat(r(6, e))
                }
            }
            var g = 0;
            for (var i = 0, len = c.length; i < len; i += 6) {
                var h = (i + 6) - len;
                if (h == 2) {
                    g = 2
                } else if (h == 4) {
                    g = 4
                }
                var j = g;
                while (--j >= 0) {
                    c.push(0)
                }
                b.push(q(c.slice(i, i + 6)))
            }
            var k = '';
            for (var i = 0, len = b.length; i < len; ++i) {
                k += n[b[i]]
            }
            for (var i = 0, len = g / 2; i < len; ++i) {
                k += '='
            }
            return k
        },
        decoder: function (a) {
            var b = a.length;
            var d = 0;
            if (a.charAt(b - 1) == '=') {
                if (a.charAt(b - 2) == '=') {
                    d = 4;
                    a = a.substring(0, b - 2)
                } else {
                    d = 2;
                    a = a.substring(0, b - 1)
                }
            }
            var e = [];
            for (var i = 0, len = a.length; i < len; ++i) {
                var c = a.charAt(i);
                for (var j = 0, size = n.length; j < size; ++j) {
                    if (c == n[j]) {
                        var f = o(j);
                        var g = f.length;
                        if (6 - g > 0) {
                            for (var k = 6 - g; k > 0; --k) {
                                f.unshift(0)
                            }
                        }
                        e = e.concat(f);
                        break
                    }
                }
            }
            if (d > 0) {
                e = e.slice(0, e.length - d)
            }
            var h = [];
            var l = [];
            for (var i = 0, len = e.length; i < len;) {
                if (e[i] == 0) {
                    h = h.concat(q(e.slice(i, i + 8)));
                    i += 8
                } else {
                    var m = 0;
                    while (i < len) {
                        if (e[i] == 1) {
                            ++m
                        } else {
                            break
                        }++i
                    }
                    l = l.concat(e.slice(i + 1, i + 8 - m));
                    i += 8 - m;
                    while (m > 1) {
                        l = l.concat(e.slice(i + 2, i + 8));
                        i += 8;
                        --m
                    }
                    h = h.concat(q(l));
                    l = []
                }
            }
            return h
        }
    };
    window.change72 = s
})();

function getDecoderString(name){
	var nn = change72.decoder(name);
	var str = '';
	for (var m = 0, len = nn.length; m < len; ++m) {
			str += String.fromCharCode(nn[m])
	}
	return str
}

function getEncoderString(name){
	var nn = change72.decoder(name);
	var str = '';
	for (var m = 0, len = nn.length; m < len; ++m) {
		str += String.fromCharCode(nn[m])
	}
	return str
}
