package com.huaxia.spider;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/bt")
public class Hello {
	@RequestMapping(value="/hello")
    public ModelAndView sendRSACode(){
		ModelAndView mv = new ModelAndView();
		mv.addObject("hello", "hello");
		mv.setViewName("show");
		return mv;
    }
}
